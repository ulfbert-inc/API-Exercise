Run ` kubectl create -f k8s/db-deployment.yaml,k8s/db-service.yaml,k8s/app-deployment.yaml,k8s/app-service.yaml` in project root to have project deployed in kubernetes cluster your `kubectl` configured against.
Notice that it takes some time for k8s to prepare resources and enable deployment (few seconds in my case). Progress is easiest to follow by inspecting DB readiness with `kubectl logs -l app=db`. Output should contain messages about successful PostgreSQL init process.

If you'd like to run project locally without kubernetes, `docker-compose up` should do the trick.


Given minikube is used as kubernetes cluster, app is easy to query with curl:
`curl http://$(minikube ip):30003/passenger/42`
or load / with browser:
`minikube service app`

For docker-compose setup use combination of localhost and port 3000 instead:
`curl -XDELETE http://localhost:3000/passenger/300`
