CREATE TABLE passengers (
	id SERIAL PRIMARY KEY,
	"Survived" INTEGER NOT NULL,
	"Pclass" INTEGER NOT NULL,
	"Name" VARCHAR NOT NULL,
	"Sex" VARCHAR NOT NULL,
	"Age" DECIMAL NOT NULL,
	"Siblings/Spouses Aboard" INTEGER NOT NULL,
	"Parents/Children Aboard" INTEGER NOT NULL,
	"Fare" DECIMAL NOT NULL
);

COPY passengers (
		"Survived", "Pclass", "Name", "Sex", "Age", "Siblings/Spouses Aboard",
		"Parents/Children Aboard", "Fare"
		)
FROM '/docker-entrypoint-initdb.d/titanic.csv' CSV HEADER;
