package main

import (
	"database/sql"
	"log"
	"fmt"
)

type passenger struct {
	ID			int     `json:"id"`
	Survived		int	`json:"survived"`
	Pclass			int	`json:"pclass"`
	Name			string	`json:"name"`
	Sex			string	`json:"sex"`
	Age			float64	`json:"age"`
	SiblingsSpousesAboard	int	`json:"siblings_spouses_aboard"`
	ParentsChildrenAboard	int	`json:"parents_children_aboard"`
	Fare			float64 `json:"fare"`
}

func (p *passenger) getPassenger(db *sql.DB) error {
	return db.QueryRow(
		"SELECT * FROM passengers WHERE id=$1",
		p.ID).Scan(
			&p.ID, &p.Survived, &p.Pclass, &p.Name, &p.Sex,
			&p.Age, &p.SiblingsSpousesAboard,
			&p.ParentsChildrenAboard, &p.Fare)
}

func (p *passenger) updatePassenger(db *sql.DB) error {
	// FIXME
	// Workaround for https://github.com/lib/pq/issues/694
        // horrible hack potentially leading SQL injection
	// should not be used in any realistic codebase
	statement := fmt.Sprintf("UPDATE passengers SET \"Survived\" = %d, " +
		"\"Pclass\" = %d, \"Name\" = '%s', \"Sex\" = '%s', \"Age\" = %f, " +
		"\"Siblings/Spouses Aboard\" = %d, \"Parents/Children Aboard\" = %d, " +
		"\"Fare\" = %f WHERE id = %d",
		p.Survived, p.Pclass, p.Name, p.Sex, p.Age,
		p.SiblingsSpousesAboard, p.ParentsChildrenAboard, p.Fare, p.ID)

	_, err :=
		db.Exec(statement)
	if err != nil {
		log.Println("postgres call error - ")
		log.Println(err)
	}
	return err
}

func (p *passenger) deletePassenger(db *sql.DB) error {
	_, err := db.Exec("DELETE FROM passengers WHERE id=$1", p.ID)
	return err
}

func (p *passenger) createPassenger(db *sql.DB) error {
	statement := fmt.Sprintf(
		"INSERT INTO passengers(\"Survived\", \"Pclass\", \"Name\", " +
		"\"Sex\", \"Age\", \"Siblings/Spouses Aboard\", " +
		"\"Parents/Children Aboard\", \"Fare\") " +
		"VALUES(%d, %d, '%s', '%s', %f, %d, %d, %f) RETURNING id",
		p.Survived, p.Pclass, p.Name, p.Sex, p.Age,
		p.SiblingsSpousesAboard, p.ParentsChildrenAboard, p.Fare)

	err := db.QueryRow(statement).Scan(&p.ID)

	if err != nil {
		return err
	}

	return nil
}
