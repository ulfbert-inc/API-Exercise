package main

import "fmt"

func main() {
	a := App{}
	a.Initialize()

	fmt.Print("API: up and running\n")
	a.Run("0.0.0.0")

}
