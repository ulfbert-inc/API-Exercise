package main

import (
	"database/sql"
	"encoding/json"
	"log"
	"net/http"
	"strconv"
	"os"

	"github.com/gorilla/mux"
	_ "github.com/lib/pq"
)

// App represents the application
type App struct {
	Router *mux.Router
	DB     *sql.DB
}

func (a *App) Initialize() {
	connectionString := os.Getenv("DB_URL")

	var err error
	a.DB, err = sql.Open("postgres", connectionString)
	if err != nil {
		log.Fatal(err)
	}

	a.Router = mux.NewRouter()
	a.initializeRoutes()
}

func (a *App) Run(addr string) {
	log.Fatal(http.ListenAndServe(":3000", a.Router))
}

func (a *App) initializeRoutes() {
	a.Router.HandleFunc("/", a.helloWorld).Methods("GET")
	a.Router.HandleFunc("/passenger", a.createPassenger).Methods("POST")
	a.Router.HandleFunc("/passenger/{id:[0-9]+}", a.getPassenger).Methods("GET")
	a.Router.HandleFunc("/passenger/{id:[0-9]+}", a.updatePassenger).Methods("PUT")
	a.Router.HandleFunc("/passenger/{id:[0-9]+}", a.deletePassenger).Methods("DELETE")
}

func (a *App) helloWorld(w http.ResponseWriter, r *http.Request) {
	respondWithJSON(w, http.StatusOK, "hello world")
}

func (a *App) createPassenger(w http.ResponseWriter, r *http.Request) {
	var p passenger
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&p); err != nil {
		respondWithError(w, http.StatusBadRequest, "invalid request payload")
		return
	}
	defer r.Body.Close()

	if err := p.createPassenger(a.DB); err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	respondWithJSON(w, http.StatusCreated, p)
}

func (a *App) getPassenger(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		respondWithError(w, http.StatusBadRequest, "invalid passenger id")
		return
	}

	p := passenger{ID: id}
	if err := p.getPassenger(a.DB); err != nil {
		switch err {
		case sql.ErrNoRows:
			respondWithError(w, http.StatusNotFound, "passenger not found")
		default:
			respondWithError(w, http.StatusInternalServerError, err.Error())
		}
		return
	}

	respondWithJSON(w, http.StatusOK, p)
}

func (a *App) updatePassenger(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		respondWithError(w, http.StatusBadRequest, "invalid passenger id")
		return
	}

	var p passenger
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&p); err != nil {
		respondWithError(w, http.StatusBadRequest, "invalid request payload")
		return
	}
	defer r.Body.Close()
	p.ID = id

	if err := p.updatePassenger(a.DB); err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	respondWithJSON(w, http.StatusOK, p)
}

func (a *App) deletePassenger(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		respondWithError(w, http.StatusBadRequest, "invalid passenger id")
		return
	}

	p := passenger{ID: id}
	if err := p.deletePassenger(a.DB); err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	respondWithJSON(w, http.StatusOK, map[string]string{"result": "success"})
}

func respondWithError(w http.ResponseWriter, code int, message string) {
	respondWithJSON(w, code, map[string]string{"error": message})
}

func respondWithJSON(w http.ResponseWriter, code int, payload interface{}) {
	response, _ := json.Marshal(payload)

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write(response)
}
